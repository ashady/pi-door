var express = require('express');

var app = express();

var gpio = require('rpi-gpio');

var gpio = require('rpi-gpio');
var async = require('async');
var sqlite3 = require('sqlite3').verbose();

// connect to local DB
var db = new sqlite3.Database('./dooraccess.sqlite',  function (err) {
	if (err) {
		console.error("DB Error", err);
	}else{
		console.log('DB Success');
		initDB();
	};
});

function initDB () {
	console.log("initDB");
	db.all("CREATE TABLE IF NOT EXISTS access (id INTEGER, token TEXT UNIQUE, email TEXT ,door  INTEGER DEFAULT 1, PRIMARY KEY(id));", function(err, rows) {
        if (err) {
            console.error("DB Error", err);
        }else{
        	console.log("DB Table created", rows);
        };
    }); 
}


var doorOne = 11,
	doorTwo = 13,
	pressOne = 8,
	pressTwo = 22,
	outOne	= 16,
	outTwo	= 18;

var open = false,
	close= true;

var doorOneOpened = false,
	doorTwoOpened = false;

var DoorOpenDuration = 10000; 		// 20 sec

pinsSetup();

function pinsSetup () {
	async.parallel([
	    function(callback) {
	        gpio.setup(doorOne, gpio.DIR_OUT, callback)
	    },
	    function(callback) {
	        gpio.setup(doorTwo, gpio.DIR_OUT, callback)
	    },
	    function(callback) {
	        gpio.setup(outOne, gpio.DIR_OUT, callback)
	    },
	    function(callback) {
	        gpio.setup(outTwo, gpio.DIR_OUT, callback)
	    },
	    function(callback) {
	        gpio.setup(pressOne, gpio.DIR_IN, gpio.EDGE_BOTH, callback)
	    },
	    function(callback) {
	        gpio.setup(pressTwo, gpio.DIR_IN, gpio.EDGE_BOTH, callback)
	    }
	], function(err, results) {
	    console.log('Pins set up');
	    pinsDefaults();
	});

}

function pinsDefaults () {
	async.parallel([
	    function(callback) {
	        gpio.write(doorOne, close, callback)
	    },
	    function(callback) {
	        gpio.write(doorTwo, close, callback)
	    },
	    function(callback) {
	        gpio.write(outOne, close, callback)
	    },
	    function(callback) {
	        gpio.write(outTwo, close, callback)
	    }
	], function(err, results) {
	    console.log('Pins defaults');
	    gpio.on('change', function(channel, value) {
    		console.log('Channel ' + channel + ' value is now ' + value);
    		checkPress(channel, value);
		});
	});
}

function checkPress (channel, value) {
	if (value == false) {
		return;
	};

	if (channel == pressOne && doorOneOpened == false) {
		doorOneOpened = true;
		openDoor(doorOne, function (err, result) {
			// body...
		});
	};

	if (channel == pressTwo && doorTwoOpened == false) {
		doorTwoOpened = true;
		openDoor(doorTwo, function (err, result) {
			// body...
		});
	};
}



function openDoor (door, next) {
	if (door == doorOne) {
		doorOneOpened = true;
	}else{
		doorTwoOpened = true;
	};

 	gpio.write(door, open, function(err) {
        if (err) {
        	console.log("err", err);
        	return next(err, null);
        }
        
        console.log('Door is opened', door);
        next(null, true); 

        //close door
        setTimeout(function() {
        	if (door == doorOne) {
				doorOneOpened = false;
			}else{
				doorTwoOpened = false;
			};
	        gpio.write(door, close, function(err) {
		        if (err) {
		        	console.log("err", err);
		        	return ;
		        };
		        console.log('Door is closed', door);
		        return ;
		    });
	    }, DoorOpenDuration); 
        
    });
 }


function setupPin (req, res, next) {
	var dir = req.params.dir;
	var pin = req.params.id;
	console.log("setupPin", pin, dir);

	dir = (dir == "in")?gpio.DIR_IN:gpio.DIR_OUT;
	gpio.setup(pin, dir, function (err) {
		console.log('Pins set up:', pin, dir, err);
		if (!err) {
			gpio.write(pin, 0, function (err) {
				// body...
			})
		};
		return  res.json({});
	})
}

function setPin (req, res, next) {
	var pin = req.params.id;
	var value = req.params.value;

	value = (value == 0)?false:true;

	gpio.write(pin, value, function(err) {
        if (err) {
        	// throw err;
        	console.log("err", err);
        	return res.status(400).json({msg: err});
        }
        console.log('Written to pin', pin, value);
        return  res.json({});
    });
}


function destroyPin (req, res, next) {
	// body...
}

function accessRequest (req, res, next) {
	var index = req.params.index;
	var token = req.params.token;

	console.log("askAccess", index, token);

	var door = (index == 1)?doorOne:doorTwo;

	db.all("SELECT * FROM access WHERE token = ?", token, function(err, rows) {
        if (err) {
            
        }else{
        	var hasAccess = false;
        	rows.forEach(function (item) {
        		if (item.door == index) {
        			hasAccess = true;
        		};
        	})
        	if (hasAccess) {
        		openDoor(door, function (err, result) {
					if (err) {
						return res.status(400).json({msg: err});
					};
					return  res.json({});
				});
        	}else{
        		return res.status(400).json({msg: "Sorry, you have no access to this door"});
        	};
        };
    }); 
}


// Tokens
function getToken (req, res, next) {
	var token = req.params.token;
	db.all("SELECT * FROM access WHERE token = ?", token, function(err, rows) {
        if (err) {
            return res.status(400).json({msg: err});
        }else{
        	return  res.json({result: rows});
        };
    }); 
}

function addNewToken (req, res, next) {
	var token = req.params.token;
	var door = req.params.door;
	db.all("insert into access (token, door) values (?,?)", [token, door], function(err, rows) {
        if (err) {
            return res.status(400).json({msg: err});
        }else{
        	return  res.json({result: rows});
        };
    });
}

function deleteToken (req, res, next) {
	var token = req.params.token;
	db.all("delete from access where token = ?", [token], function(err, rows) {
        if (err) {
            return res.status(400).json({msg: err});
        }else{
        	return  res.json({result: rows});
        };
    });
}

var router = new express.Router();

router.post('/pin/:id/:dir', setupPin);
router.put('/pin/:id/:value', setPin);
router.delete('pin/:id', destroyPin);


router.post('/access/door/:index/:token', accessRequest);


router.post('/door/one/open', function (req, res, next) {
	openDoor(doorOne, function (err, result) {
		if (err) {
			return res.status(400).json({msg: err});
		};
		return  res.json({});
	});
});

router.post('/door/two/open', function (req, res, next) {
	openDoor(doorTwo, function (err, result) {
		if (err) {
			return res.status(400).json({msg: err});
		};
		return  res.json({});
	});
});

router.get('/access/token/:token', getToken);
router.post('/access/token/:token/:door', addNewToken);
router.delete('/access/token/:token', deleteToken);

app.use('/', router);

app.all('*', function(req, res) {
        console.log("GET *");
        return  res.status(404).json({});
    });


// Start server
var server = app.listen(3000, function() {
    console.log('Listening on port %d', server.address().port);
});